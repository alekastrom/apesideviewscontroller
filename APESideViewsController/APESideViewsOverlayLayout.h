//
//  APESideViewsOverlayLayout.h
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-19.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsLayout.h"

FOUNDATION_EXPORT NSString *const APEOverlayLayoutLeftHairlineDecoration;
FOUNDATION_EXPORT NSString *const APEOverlayLayoutRightHairlineDecoration;

@interface APESideViewsOverlayLayout : APESideViewsLayout

@end
