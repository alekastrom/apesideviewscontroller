//
//  APESideViewsViewBasedAnimation.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-26.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsViewBasedAnimation.h"

@implementation APESideViewsViewBasedAnimation {
    NSTimeInterval _duration;
}

const CGFloat APEInteractiveDuration = 0.2f;
const CGFloat APENonInteractiveDuration = 0.3f;

- (id)init {
    self = [super init];
    if (self) {
        _duration = APENonInteractiveDuration;
        _showDampening = 0.85;
    }
    return self;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    [self animateSideViewsTransition:(id<APESideViewsContextTransitioning>)transitionContext];
}
- (void)animateSideViewsTransition:(id<APESideViewsContextTransitioning>)transitionContext {
    
    _transitionContext = transitionContext;
    
    NSArray *vcs = @[[transitionContext viewControllerForKey:APESideViewsTransitionContextMainViewControllerKey], [transitionContext viewControllerForKey:APESideViewsTransitionContextOperatingViewControllerKey]];
    
    for (UIViewController *vc in vcs) {
        if (vc.view.superview == nil) {
            [[transitionContext containerView] addSubview:vc.view];
        }
        [vc.view setLayoutAttributes:[self beginAnimationAttributesForViewController:vc initialAttributes:[transitionContext initialAttributesForViewController:vc] finalAttributes:[transitionContext finalAttributesForViewController:vc]]];
    }
    
    NSDictionary *decorationViews = [transitionContext decorationViews];
    [decorationViews enumerateKeysAndObjectsUsingBlock:^(NSString *identifier, UIView *view, BOOL *stop) {
        APESideViewsLayoutAttributes *initAttributes = [transitionContext initialAttributesForDecorationViews][identifier];
        APESideViewsLayoutAttributes *finalAttributes = [transitionContext finalAttributesForDecorationViews][identifier];
        
        if (!initAttributes) {
            [[transitionContext containerView] addSubview:view];
        }
        [view setLayoutAttributes:[self beginAnimationAttributesForDecorationView:view identifier:identifier initialAttributes:initAttributes finalAttributes:finalAttributes]];
    }];

    void (^animations)() = ^{
        for (UIViewController *vc in vcs) {
            [vc.view setLayoutAttributes:[self endAnimationAttributesForViewController:vc initialAttributes:[transitionContext initialAttributesForViewController:vc] finalAttributes:[transitionContext finalAttributesForViewController:vc]]];
        }
        
        NSDictionary *decorationViews = [transitionContext decorationViews];
        [decorationViews enumerateKeysAndObjectsUsingBlock:^(NSString *identifier, UIView *view, BOOL *stop) {
            APESideViewsLayoutAttributes *initAttributes = [transitionContext initialAttributesForDecorationViews][identifier];
            APESideViewsLayoutAttributes *finalAttributes = [transitionContext finalAttributesForDecorationViews][identifier];
            
            [view setLayoutAttributes:[self endAnimationAttributesForDecorationView:view identifier:identifier initialAttributes:initAttributes finalAttributes:finalAttributes]];
        }];
    };
    
    void (^completion)(BOOL completed) = ^(BOOL finished){
        if (finished) {
            [transitionContext completeTransition:YES];
        }
    };
    
    
    if ([transitionContext isInteractive]) {
        _duration = APEInteractiveDuration;
        // Use a linear animation curve when interacting for direct manipulation, faster duration
        [UIView animateWithDuration:_duration delay:0 options:UIViewAnimationOptionCurveLinear animations:animations completion:completion];
    } else {
        _duration = APENonInteractiveDuration;
        [UIView animateWithDuration:_duration delay:0 usingSpringWithDamping:[self springDampening] initialSpringVelocity:0 options:0 animations:animations completion:completion];
    }
}

- (CGFloat)springDampening {
    if ([self.transitionContext operation] == APESideViewsControllerOperationHideSideView) {
        return 1;
    } else {
        return self.showDampening;
    }
}

- (void)animationEnded:(BOOL)transitionCompleted {
    
}

- (NSTimeInterval)transitionDuration:(id<APESideViewsContextTransitioning>)transitionContext {
    return _duration;
}

- (APESideViewsLayoutAttributes *)beginAnimationAttributesForViewController:(UIViewController *)viewController initialAttributes:(APESideViewsLayoutAttributes *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes {
    if (initialAttributes) {
        return initialAttributes;
    } else {
        return finalAttributes;
    }
}
- (APESideViewsLayoutAttributes *)endAnimationAttributesForViewController:(UIViewController *)viewController initialAttributes:(APESideViewsLayoutAttributes *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes {
    if (finalAttributes) {
        return finalAttributes;
    } else {
        return initialAttributes;
    }
}
- (APESideViewsLayoutAttributes *)beginAnimationAttributesForDecorationView:(UIView *)decorationView identifier:(NSString *)identifier initialAttributes:(APESideViewsLayoutAttributes *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes {
    if (initialAttributes) {
        return initialAttributes;
    } else {
        return finalAttributes;
    }
}
- (APESideViewsLayoutAttributes *)endAnimationAttributesForDecorationView:(UIView *)decorationView identifier:(NSString *)identifier initialAttributes:(APESideViewsLayoutAttributes *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes {
    if (finalAttributes) {
        return finalAttributes;
    } else {
        return initialAttributes;
    }
}

@end
