//
//  APESideViewsFadeAnimation.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-26.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsFadeAnimation.h"

@implementation APESideViewsFadeAnimation

- (APESideViewsLayoutAttributes *)beginAnimationAttributesForViewController:(UIViewController *)viewController initialAttributes:(APESideViewsLayoutAttributes *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes {
    if (initialAttributes) {
        return initialAttributes;
    } else {
        APESideViewsMutableLayoutAttributes *attributes = [finalAttributes mutableCopy];
        attributes.alpha = 0;
        return attributes;
    }
}
- (APESideViewsLayoutAttributes *)endAnimationAttributesForViewController:(UIViewController *)viewController initialAttributes:(APESideViewsLayoutAttributes *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes {
    if (finalAttributes) {
        return finalAttributes;
    } else {
        APESideViewsMutableLayoutAttributes *attributes = [initialAttributes mutableCopy];
        attributes.alpha = 0;
        return attributes;
    }
}
- (APESideViewsLayoutAttributes *)beginAnimationAttributesForDecorationView:(UIView *)decorationView identifier:(NSString *)identifier initialAttributes:(APESideViewsLayoutAttributes *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes {
    if (initialAttributes) {
        return initialAttributes;
    } else {
        APESideViewsMutableLayoutAttributes *attributes = [finalAttributes mutableCopy];
        attributes.alpha = 0;
        return attributes;
    }
}
- (APESideViewsLayoutAttributes *)endAnimationAttributesForDecorationView:(UIView *)decorationView identifier:(NSString *)identifier initialAttributes:(APESideViewsLayoutAttributes *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes {
    if (finalAttributes) {
        return finalAttributes;
    } else {
        APESideViewsMutableLayoutAttributes *attributes = [initialAttributes mutableCopy];
        attributes.alpha = 0;
        return attributes;
    }
}

@end
