//
//  APESideViewsOverlaySlideAnimation.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-26.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsOverlaySlideAnimation.h"
#import "APESideViewsOverlayLayout.h"

@implementation APESideViewsOverlaySlideAnimation {
    CGFloat _hairLineWidth;
}

- (APESideViewsLayoutAttributes *)beginAnimationAttributesForViewController:(UIViewController *)viewController initialAttributes:(APESideViewsLayoutAttributes *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes {
    if (initialAttributes) {
        return initialAttributes;
    } else {
        APESideViewsMutableLayoutAttributes *attributes = [finalAttributes mutableCopy];
        CGRect frame = attributes.frame;
        CGFloat totalWidth = frame.size.width + [self hairLineWidth];
        frame.origin.x += ([self.transitionContext transitionSide] == APESideViewsTransitionSideLeft) ? - totalWidth : totalWidth;
        attributes.frame = frame;
        return attributes;
    }
}
- (APESideViewsLayoutAttributes *)endAnimationAttributesForViewController:(UIViewController *)viewController initialAttributes:(APESideViewsLayoutAttributes *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes {
    if (finalAttributes) {
        return finalAttributes;
    } else {
        APESideViewsMutableLayoutAttributes *attributes = [initialAttributes mutableCopy];
        CGRect frame = attributes.frame;
        CGFloat totalWidth = frame.size.width + [self hairLineWidth];
        frame.origin.x += ([self.transitionContext transitionSide] == APESideViewsTransitionSideLeft) ? - totalWidth : totalWidth;
        attributes.frame = frame;
        return attributes;
    }
}
- (APESideViewsLayoutAttributes *)beginAnimationAttributesForDecorationView:(UIView *)decorationView identifier:(NSString *)identifier initialAttributes:(APESideViewsLayoutAttributes *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes {
    if (initialAttributes) {
        return initialAttributes;
    } else {
        APESideViewsMutableLayoutAttributes *attributes = [finalAttributes mutableCopy];
        CGRect frame = attributes.frame;
        
        if ([self.transitionContext transitionSide] == APESideViewsTransitionSideLeft) {
            frame.origin.x = -frame.size.width;
        } else {
            APESideViewsLayoutAttributes *mainAttributes = [self.transitionContext finalAttributesForViewController:[self.transitionContext viewControllerForKey:APESideViewsTransitionContextMainViewControllerKey]];
            frame.origin.x = mainAttributes.frame.size.width;
        }
        attributes.frame = frame;
        return attributes;
    }
}
- (APESideViewsLayoutAttributes *)endAnimationAttributesForDecorationView:(UIView *)decorationView identifier:(NSString *)identifier initialAttributes:(APESideViewsLayoutAttributes *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes {
    if (finalAttributes) {
        return finalAttributes;
    } else {
        APESideViewsMutableLayoutAttributes *attributes = [initialAttributes mutableCopy];
        CGRect frame = attributes.frame;
        
        if ([self.transitionContext transitionSide] == APESideViewsTransitionSideLeft) {
            frame.origin.x = -frame.size.width;
        } else {
            APESideViewsLayoutAttributes *mainAttributes = [self.transitionContext finalAttributesForViewController:[self.transitionContext viewControllerForKey:APESideViewsTransitionContextMainViewControllerKey]];
            frame.origin.x = mainAttributes.frame.size.width;
        }
        attributes.frame = frame;
        return attributes;
    }
}

- (CGFloat)hairLineWidth {
    if (!_hairLineWidth) {
        
        NSString *identifier = ([self.transitionContext transitionSide] == APESideViewsTransitionSideLeft) ? APEOverlayLayoutLeftHairlineDecoration : APEOverlayLayoutRightHairlineDecoration;
                
        APESideViewsLayoutAttributes *layoutAttributes;
        if ([self.transitionContext operation] == APESideViewsControllerOperationShowSideView) {
            layoutAttributes = [self.transitionContext finalAttributesForDecorationViews][identifier];
        } else {
            layoutAttributes = [self.transitionContext initialAttributesForDecorationViews][identifier];
        }
        
        _hairLineWidth = layoutAttributes.frame.size.width;
    }
    return _hairLineWidth;
}

@end
