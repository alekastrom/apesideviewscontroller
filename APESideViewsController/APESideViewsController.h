//
//  APESideViewsController.h
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-15.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

@import UIKit;
#import "APESideViewsLayoutAttributes.h"
#import "APESideViewsPanInteractionController.h"

typedef NS_ENUM(NSUInteger, APESideViewsControllerOperation) {
    APESideViewsControllerOperationNone = 0,
    APESideViewsControllerOperationShowSideView,
    APESideViewsControllerOperationHideSideView
};

#define APESideMainSegueIdentifier @"APESideMainView"
#define APESideLeftSegueIdentifier @"APESideLeftView"
#define APESideRightSegueIdentifier @"APESideRightView"

extern NSString *const APESideViewsControllerWillPerformOperationNotification;
extern NSString *const APESideViewsControllerDidPerformOperationNotification;
extern NSString *const APESideViewsControllerNotificationOperationKey;
extern NSString *const APESideViewsControllerNotificationViewControllerKey;

@class APESideViewsLayout;
@protocol APESideViewsControllerDelegate;
@interface APESideViewsController : UIViewController

@property (nonatomic, strong) APESideViewsLayout *layout;
@property (nonatomic, weak) id<APESideViewsControllerDelegate> delegate;

@property (nonatomic, strong) UIViewController *leftViewController;
@property (nonatomic, strong) UIViewController *mainViewController;
@property (nonatomic, strong) UIViewController *rightViewController;

@property (nonatomic, getter=isShowingLeftViewController) BOOL showingLeftViewController;
@property (nonatomic, getter=isShowingRightViewController) BOOL showingRightViewController;
@property (nonatomic, getter=isShowingMainViewController, readonly) BOOL showingMainViewController;

@property (nonatomic) BOOL disablesMainInteractionWhenShowingSideView; // Defaults to YES
@property (nonatomic, readonly) APESideViewsPanInteractionController *defaultInteractionController;

@property (nonatomic) BOOL operationsDisabled;

- (BOOL)isShowingViewController:(UIViewController *)viewController;
- (void)setShowingLeftViewController:(BOOL)showingLeft animated:(BOOL)animated;
- (void)setShowingRightViewController:(BOOL)showingLeft animated:(BOOL)animated;
- (void)setShowing:(BOOL)showing sideViewController:(UIViewController *)sideViewController animated:(BOOL)animated;
- (void)showMainViewControllerAnimated:(BOOL)animated;

- (void)setLayout:(APESideViewsLayout *)layout animated:(BOOL)animated;

// Hooks for subclasses
- (void)willPerformOperation:(APESideViewsControllerOperation)operation onViewController:(UIViewController *)viewController;
- (void)didPerformOperation:(APESideViewsControllerOperation)operation onViewController:(UIViewController *)viewController;

@end

@protocol APESideViewsAnimatedTransitioning;
@protocol APESideViewsControllerDelegate <NSObject>
@optional
- (void)sideViewsController:(APESideViewsController *)sideViewsController willPerformOperation:(APESideViewsControllerOperation)operation onViewController:(UIViewController *)viewController;
- (void)sideViewsController:(APESideViewsController *)sideViewsController didPerformOperation:(APESideViewsControllerOperation)operation onViewController:(UIViewController *)viewController;

- (id<APESideViewsAnimatedTransitioning>)sideViewsController:(APESideViewsController *)sideViewsController animationControllerForOperation:(APESideViewsControllerOperation)operation onViewController:(UIViewController *)viewController;
- (id<UIViewControllerInteractiveTransitioning>)sideViewsController:(APESideViewsController *)sideViewsController interactionControllerForOperation:(APESideViewsControllerOperation)operation onViewController:(UIViewController *)viewController;


@end

/// UIViewController category
@interface UIViewController (APESideViewsController)
@property (nonatomic, readonly) APESideViewsController *sideViewsController;
@end

// Transitioning protocols
FOUNDATION_EXPORT NSString *const APESideViewsTransitionContextMainViewControllerKey;
FOUNDATION_EXPORT NSString *const APESideViewsTransitionContextOperatingViewControllerKey;

typedef NS_ENUM(NSUInteger, APESideViewsTransitionSide) {
    APESideViewsTransitionSideLeft,
    APESideViewsTransitionSideRight
};

@protocol APESideViewsAnimatedTransitioning;
@protocol APESideViewsContextTransitioning <UIViewControllerContextTransitioning>

- (APESideViewsLayoutAttributes *)initialAttributesForViewController:(UIViewController *)viewController;
- (APESideViewsLayoutAttributes *)finalAttributesForViewController:(UIViewController *)viewController;

- (NSDictionary *)decorationViews;
- (NSDictionary *)initialAttributesForDecorationViews;
- (NSDictionary *)finalAttributesForDecorationViews;

- (APESideViewsControllerOperation)operation;
- (APESideViewsTransitionSide)transitionSide;

- (id<APESideViewsAnimatedTransitioning>)animator;

@end

@protocol APESideViewsAnimatedTransitioning <UIViewControllerAnimatedTransitioning>

- (void)animateSideViewsTransition:(id<APESideViewsContextTransitioning>)transitionContext;
- (void)animationEnded:(BOOL)transitionCompleted;
- (NSTimeInterval)transitionDuration:(id<APESideViewsContextTransitioning>)transitionContext;

@end

