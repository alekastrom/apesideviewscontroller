//
//  APESideViewsPanInteractionController.h
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-10-09.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, APESideViewsPanArea) {
    APESideViewsPanAreaNone = 0,
    APESideViewsPanAreaEdges,
    APESideViewsPanAreaEdgesNavigationBar,
    APESideViewsPanAreaFull
};

@class APESideViewsController;
@interface APESideViewsPanInteractionController : NSObject <UIViewControllerInteractiveTransitioning>

@property (nonatomic, readonly) UIPanGestureRecognizer *gestureRecognizer;
@property (nonatomic) APESideViewsPanArea panArea; // Area used for opening/closing side views, defaults to APESideViewsPanAreaEdgesNavigationBar

- (id)initWithSideViewsController:(APESideViewsController *)sideViewsController;

- (BOOL)isInteracting;

@end
