//
//  APESideViewsLayout.h
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-18.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

@import Foundation;
#import "APESideViewsLayoutAttributes.h"
#import "APESideViewsController.h"

@interface APESideViewsLayout : NSObject

@property (nonatomic, readonly) APESideViewsController *sideViewsController;
@property (nonatomic, readonly) APESideViewsControllerOperation currentOperation;
@property (nonatomic, readonly) UIViewController *operatingViewController;
@property (nonatomic, getter=usingStatusBarStyleFromPresentedSideViewController) BOOL useStatusBarStyleFromPresentedSideViewController;

- (instancetype)initWithSideViewsController:(APESideViewsController *)sideViewsController;

- (void)prepareLayout;
- (void)invalidateLayout;

- (APESideViewsControllerOperation)currentOperation;
- (UIViewController *)operatingViewController;

- (APESideViewsLayoutAttributes *)layoutAttributesForViewController:(UIViewController *)viewController;
- (NSDictionary *)layoutAttributesForDecorationViews;

- (void)prepareForOperation:(APESideViewsControllerOperation)operation onViewController:(UIViewController *)viewController;
- (APESideViewsLayoutAttributes *)finalLayoutAttributesForViewController:(UIViewController *)viewController;
- (NSDictionary *)finalLayoutAttributesForDecorationViews;
- (void)finalizeOperation;

- (void)registerClass:(Class)class forDecorationViewWithIdentifier:(NSString *)decorationViewIdentifier;

- (UIViewController *)childViewControllerForStatusBarStyle;

@end
