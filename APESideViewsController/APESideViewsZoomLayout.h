//
//  APESideViewsZoomLayout.h
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-28.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsSlideOverLayout.h"
#import "APESideViewsViewBasedAnimation.h"

@interface APESideViewsZoomLayout : APESideViewsSlideOverLayout

+ (APESideViewsViewBasedAnimation *)zoomAnimation;

@end
