//
//  APESideViewsPanInteractionController.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-10-09.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsPanInteractionController.h"
#import "APESideViewsController.h"

const CGFloat APESideViewsEdgePanMargin = 30.0f;

typedef NS_ENUM(NSUInteger, APESideViewsPanLocation) {
    APESideViewsPanLocationOutsideMainView = 0,
    APESideViewsPanLocationNavigationBar,
    APESideViewsPanLocationLeftEdge,
    APESideViewsPanLocationRightEdge,
    APESideViewsPanLocationBetweenEdges
};

@interface APESideViewsPanInteractionController()<UIGestureRecognizerDelegate>
@end

@implementation APESideViewsPanInteractionController {
    APESideViewsController *_sideViewsController;
    UIPanGestureRecognizer *_panRecognizer;
    id<APESideViewsContextTransitioning> _transitionContext;
    BOOL _isInteracting;
    CADisplayLink *_displayLink;
}

- (id)initWithSideViewsController:(APESideViewsController *)sideViewsController {
    self = [super init];
    if (self) {
        _sideViewsController = sideViewsController;
        _panArea = APESideViewsPanAreaEdgesNavigationBar;
        _panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        _panRecognizer.delegate = self;
    }
    return self;
}

- (UIPanGestureRecognizer *)gestureRecognizer {
    return _panRecognizer;
}

- (BOOL)isInteracting {
    return _isInteracting;
}

- (void)startInteractiveTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    _transitionContext = (id<APESideViewsContextTransitioning>)transitionContext;
    [_transitionContext containerView].layer.speed = 0;

    [[_transitionContext animator] animateSideViewsTransition:_transitionContext];
}

- (BOOL)leftToRightAnimation {
    if ([_transitionContext operation] == APESideViewsControllerOperationShowSideView && [_transitionContext transitionSide] == APESideViewsTransitionSideLeft) {
        return YES;
    } else if ([_transitionContext operation] == APESideViewsControllerOperationHideSideView && [_transitionContext transitionSide] == APESideViewsTransitionSideRight) {
        return YES;
    } else {
        return NO;
    }
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    CGPoint translation = [recognizer translationInView:_sideViewsController.view];
    CGPoint velocity = [recognizer velocityInView:_sideViewsController.view];
    
    CGFloat progress = translation.x/280;
    if (![self leftToRightAnimation]) {
        progress = -progress;
    }
    progress = MAX(progress, 0);
    
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan: {
            
            BOOL rightPanDirection = velocity.x > 0;
            BOOL leftPanDirection = !rightPanDirection;
            
            _isInteracting = YES;
            
            if (_sideViewsController.showingLeftViewController && leftPanDirection) {
                [_sideViewsController setShowingLeftViewController:NO animated:YES];
            } else if (_sideViewsController.showingRightViewController && rightPanDirection) {
                [_sideViewsController setShowingRightViewController:NO animated:YES];
            } else if (_sideViewsController.showingMainViewController) {
                if (rightPanDirection && _sideViewsController.leftViewController) {
                    [_sideViewsController setShowingLeftViewController:YES animated:YES];
                } else if (leftPanDirection && _sideViewsController.rightViewController) {
                    [_sideViewsController setShowingRightViewController:YES animated:YES];
                }
            }
            
        } break;
            
        case UIGestureRecognizerStateChanged: {
            [self setTimeOffset:progress*[[_transitionContext animator] transitionDuration:_transitionContext]];
        } break;
            
        case UIGestureRecognizerStateFailed:
        case UIGestureRecognizerStateEnded:
        {
            _isInteracting = NO;
            
            if ((velocity.x < 0 && [self leftToRightAnimation]) || (velocity.x > 0 && ![self leftToRightAnimation])) {
                [self cancelInteractiveTransition];
            } else {
                [self finishInteractiveTransition];
            }

        } break;
            
        default:
            break;
    }
}

- (APESideViewsPanLocation)locationOfPan:(UIGestureRecognizer *)panGestureRecognizer {
    CGPoint location = [panGestureRecognizer locationInView:panGestureRecognizer.view];

    if (location.y <= 64.0f) {
        return APESideViewsPanLocationNavigationBar;
    } else if (location.x < APESideViewsEdgePanMargin) {
        return APESideViewsPanLocationLeftEdge;
    } else if (location.x > panGestureRecognizer.view.bounds.size.width - APESideViewsEdgePanMargin) {
        return APESideViewsPanLocationRightEdge;
    } else {
        return APESideViewsPanLocationBetweenEdges;
    }
}

- (void)setTimeOffset:(NSTimeInterval)timeOffset {
    [_transitionContext containerView].layer.timeOffset = timeOffset;
}
- (CFTimeInterval)timeOffset {
    return [_transitionContext containerView].layer.timeOffset;
}

- (void)finishInteractiveTransition {
    CALayer *layer = [_transitionContext containerView].layer;
    
    layer.speed = 1;
    
    CFTimeInterval pausedTime = [layer timeOffset];
    layer.timeOffset = 0.0;
    layer.beginTime = 0.0;
    CFTimeInterval timeSincePause = [layer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
    layer.beginTime = timeSincePause;
}

- (void)cancelInteractiveTransition {
    _displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(tickCancelAnimation)];
    [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
}

- (void)tickCancelAnimation {
    NSTimeInterval timeOffset = [self timeOffset]-[_displayLink duration];
    if (timeOffset < 0) {
        [self transitionFinishedCanceling];
    } else {
        [self setTimeOffset:timeOffset];
    }
}

- (void)transitionFinishedCanceling {
    [_displayLink invalidate];
    
    CALayer *layer = [_transitionContext containerView].layer;
    layer.speed = 1;

    [_transitionContext cancelInteractiveTransition];
    [_transitionContext completeTransition:NO];
}

#pragma mark - Gesture recognizer delegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (_sideViewsController.showingMainViewController) {
        UIPanGestureRecognizer *panGestureRecognizer = (UIPanGestureRecognizer *)gestureRecognizer;
        
        CGPoint translation = [panGestureRecognizer translationInView:panGestureRecognizer.view];
        if (atan(fabs(translation.y) / fabs(translation.x)) > M_PI_4/2.0) {
            return NO; // Angle too steep
        }
        
        APESideViewsPanLocation panLocation = [self locationOfPan:panGestureRecognizer];
        
        switch (self.panArea) {
            case APESideViewsPanAreaNone:
                return NO;
            case APESideViewsPanAreaEdges:
                return (panLocation == APESideViewsPanLocationLeftEdge || panLocation == APESideViewsPanLocationRightEdge);
            case APESideViewsPanAreaEdgesNavigationBar:
                return (panLocation == APESideViewsPanLocationLeftEdge || panLocation == APESideViewsPanLocationRightEdge || panLocation == APESideViewsPanLocationNavigationBar);
            case APESideViewsPanAreaFull:
                return YES;
            default:
                return NO;
        }
    }
    return YES;
}

@end
