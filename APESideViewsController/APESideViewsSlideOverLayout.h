//
//  APESideViewsSlideOverLayout.h
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-27.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsLayout.h"

FOUNDATION_EXPORT NSString *const APESlideOverLayoutLeftEdgeDecoration;
FOUNDATION_EXPORT NSString *const APESlideOverLayoutRightEdgeDecoration;
FOUNDATION_EXPORT NSString *const APESlideOverLayoutStatusBarDecoration;

typedef NS_ENUM(NSUInteger, APESlideOpenSide) {
    APESlideOpenSideNone,
    APESlideOpenSideLeft,
    APESlideOpenSideRight
};

@interface APESideViewsSlideOverLayout : APESideViewsLayout

// Hooks
- (APESideViewsLayoutAttributes *)mainAttributesWithOpenSide:(APESlideOpenSide)openSide;
- (APESideViewsLayoutAttributes *)sideViewControllerAttributesWhenShowing:(BOOL)showing;

@end
