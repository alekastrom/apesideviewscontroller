//
//  APESetSideViewsSegue.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-15.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESetSideViewsSegue.h"
#import "APESideViewsController.h"

@implementation APESetSideViewsSegue

- (void)perform {
    APESideViewsController *sideViewsController = (APESideViewsController *)self.sourceViewController;
    
    if ([self.identifier isEqualToString:APESideLeftSegueIdentifier]) {
        sideViewsController.leftViewController = self.destinationViewController;
    } else if ([self.identifier isEqualToString:APESideMainSegueIdentifier]) {
        sideViewsController.mainViewController = self.destinationViewController;
    } else if ([self.identifier isEqualToString:APESideRightSegueIdentifier]) {
        sideViewsController.rightViewController = self.destinationViewController;
    } else {
        NSAssert(NO, @"Invalid identifier for Set Side Views Segue");
    }
}

@end
