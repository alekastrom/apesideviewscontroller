//
//  APEHairlineView.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-25.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APEHairlineView.h"

@implementation APEHairlineView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:77/255.0]; // Match navigaition bar hairline
        self.userInteractionEnabled = NO;
    }
    return self;
}

@end
