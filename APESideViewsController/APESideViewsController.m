//
//  APESideViewsController.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-15.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsController.h"
#import "APESideViewsOverlayLayout.h"
#import "APESideViewsConcreteContextTransitioning.h"
#import "APESideViewsFadeAnimation.h"
#import "APESideViewsOverlaySlideAnimation.h"
#import "APESideViewsSlideOverLayout.h"

NSString *const APESideViewsControllerWillPerformOperationNotification = @"APESideViewsControllerWillPerformOperationNotification";
NSString *const APESideViewsControllerDidPerformOperationNotification = @"APESideViewsControllerDidPerformOperationNotification";
NSString *const APESideViewsControllerNotificationOperationKey = @"operation";
NSString *const APESideViewsControllerNotificationViewControllerKey = @"viewController";

@interface APESideViewsLayout (APESideViewsControllerAdditions)
- (Class)classForDecorationViewWithIdentifier:(NSString *)decorationViewIdentifier;
@end

typedef void(^VoidBlock)();

@interface APESideViewsController () <UIViewControllerTransitioningDelegate, UIGestureRecognizerDelegate>
@property (nonatomic, strong) UIView *leftContainerView;
@property (nonatomic, strong) UIView *mainContainerView;
@property (nonatomic, strong) UIView *rightContainerView;

@property (nonatomic, strong) UITapGestureRecognizer *mainTapRecognizer;
@property (nonatomic, strong) APESideViewsPanInteractionController *defaultInteractionController;

@end

@implementation APESideViewsController {
    APESideViewsControllerOperation _currentOperation;
    UIViewController *_operatingViewController;
    NSDictionary *_decorationViews;
}
@synthesize layout = _layout;

#pragma mark - Object lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self SVC_init];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self SVC_init];
    }
    return self;
}
- (void)SVC_init {
    _disablesMainInteractionWhenShowingSideView = YES;
}

#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    BOOL needToSetupLayout = YES;
    // Load view controllers
    if (!_mainViewController) {
        @try { [self performSegueWithIdentifier:APESideMainSegueIdentifier sender:self]; needToSetupLayout = NO; }
        @catch (NSException *exception) { NSLog(@"No left view controller"); }
        @finally {}
    }
    
    if (!_leftViewController) {
        @try { [self performSegueWithIdentifier:APESideLeftSegueIdentifier sender:self]; needToSetupLayout = NO; }
        @catch (NSException *exception) { NSLog(@"No left view controller"); }
        @finally {}
    }
    
    if (!_rightViewController) {
        @try { [self performSegueWithIdentifier:APESideRightSegueIdentifier sender:self]; needToSetupLayout = NO; }
        @catch (NSException *exception) { NSLog(@"No right view controller"); }
        @finally {}
    }
    
    if (needToSetupLayout) {
        [self reloadLayout];
    }
    
    [self setupGestureRecognizers];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self reloadLayout];
}

#pragma mark - User interaction
- (void)mainViewTapped:(UITapGestureRecognizer *)recognizer {
    if (recognizer == self.mainTapRecognizer) {
        if (_showingLeftViewController) {
            [self setShowingLeftViewController:NO animated:YES];
        } else {
            [self setShowingRightViewController:NO animated:YES];
        }
    }
}
- (void)mainViewPanned:(UIPanGestureRecognizer *)recognizer {
    
}
- (void)mainViewSwiped:(UISwipeGestureRecognizer *)recognizer {
    if (!self.showingMainViewController) {
        return;
    }
    
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft && self.rightViewController) {
        [self setShowingRightViewController:YES animated:YES];
    } else if (recognizer.direction == UISwipeGestureRecognizerDirectionRight && self.leftViewController) {
        [self setShowingLeftViewController:YES animated:YES];
    }
}

#pragma mark - Accessors
- (APESideViewsLayout *)layout {
    if (!_layout) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            self.layout = [[APESideViewsSlideOverLayout alloc] initWithSideViewsController:self];
        } else {
            self.layout = [[APESideViewsOverlayLayout alloc] initWithSideViewsController:self];
        }
    }
    return _layout;
}
- (void)setLayout:(APESideViewsLayout *)layout {
    [self setLayout:layout animated:NO];
}
- (void)setLayout:(APESideViewsLayout *)layout animated:(BOOL)animated {
    if (_layout != layout) {
        _layout = layout;
        
        if (self.isViewLoaded) {
            if (animated) {
                self.view.userInteractionEnabled = NO;
                [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:1 initialSpringVelocity:0 options:0 animations:^{
                    [self reloadLayout];
                } completion:^(BOOL finished) {
                    self.view.userInteractionEnabled = YES;
                }];
            } else {
                [self reloadLayout];
            }
        }
    }
}
- (void)setMainViewController:(UIViewController *)mainViewController {
    if (_mainViewController != mainViewController) {
        [self removeViewController:_mainViewController];
        _mainViewController = mainViewController;
        [self addChildViewController:_mainViewController];
        [_mainViewController didMoveToParentViewController:self];
        if ([self isViewLoaded]) {
            [self reloadLayout];
        }
    }
}
- (void)setLeftViewController:(UIViewController *)leftViewController {
    if (_leftViewController != leftViewController) {
        [self removeViewController:_leftViewController];
        _leftViewController = leftViewController;
        [self addChildViewController:leftViewController];
        [leftViewController didMoveToParentViewController:self];
        
        if ([self isViewLoaded]) {
            [self reloadLayout];
        }
    }
}
- (void)setRightViewController:(UIViewController *)rightViewController {
    if (_rightViewController != rightViewController) {
        [self removeViewController:_rightViewController];
        _rightViewController = rightViewController;
        [self addChildViewController:rightViewController];
        [rightViewController didMoveToParentViewController:self];
        
        if ([self isViewLoaded]) {
            [self reloadLayout];
        }
    }
}
- (BOOL)isShowingViewController:(UIViewController *)viewController {
    if (viewController == _leftViewController) {
        return self.isShowingLeftViewController;
    } else if (viewController == _rightViewController) {
        return self.isShowingRightViewController;
    } else {
        return NO;
    }
}
- (UIGestureRecognizer *)interactiveGestureRecognizer {
    return self.defaultInteractionController.gestureRecognizer;
}
- (UIView *)leftContainerView {
    if (!_leftContainerView) {
        _leftContainerView = [self newContainerView];
    }
    return _leftContainerView;
}
- (UIView *)mainContainerView {
    if (!_mainContainerView) {
        _mainContainerView = [self newContainerView];
    }
    return _mainContainerView;
}
- (UIView *)rightContainerView {
    if (!_rightContainerView) {
        _rightContainerView = [self newContainerView];
    }
    return _rightContainerView;
}

#pragma mark - Show/hide view controllers
#pragma mark Left
- (void)setShowingLeftViewController:(BOOL)showingLeftViewController {
    [self setShowingLeftViewController:showingLeftViewController animated:NO];
}
- (void)setShowingLeftViewController:(BOOL)showingLeft animated:(BOOL)animated {
    [self setShowing:showingLeft sideViewController:_leftViewController animated:animated];
}
#pragma mark Right
- (void)setShowingRightViewController:(BOOL)showingRightViewController {
    [self setShowingRightViewController:showingRightViewController animated:NO];
}
- (void)setShowingRightViewController:(BOOL)showingRight animated:(BOOL)animated {
    [self setShowing:showingRight sideViewController:_rightViewController animated:animated];
}
#pragma mark Main
- (BOOL)isShowingMainViewController {
    return (!self.showingLeftViewController && !self.showingRightViewController);
}
- (void)showMainViewControllerAnimated:(BOOL)animated {
    if (self.showingLeftViewController) {
        [self setShowingLeftViewController:NO animated:animated];
    } else if (self.showingRightViewController) {
        [self setShowingRightViewController:NO animated:animated];
    }
}

#pragma mark Both sides
- (void)setShowing:(BOOL)showing sideViewController:(UIViewController *)sideViewController animated:(BOOL)animated {
    
    BOOL *showingIvar = (_leftViewController == sideViewController) ? &_showingLeftViewController : &_showingRightViewController;
    
    if (showing != *showingIvar) {
        if ([self isViewLoaded]) {
            APESideViewsControllerOperation operation = showing ? APESideViewsControllerOperationShowSideView : APESideViewsControllerOperationHideSideView;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:APESideViewsControllerWillPerformOperationNotification object:self userInfo:@{APESideViewsControllerNotificationOperationKey: @(operation), APESideViewsControllerNotificationViewControllerKey: sideViewController}];
            
            [self performOperation:operation withViewController:sideViewController animated:animated completion:^{
                *showingIvar = showing;
                [[NSNotificationCenter defaultCenter] postNotificationName:APESideViewsControllerDidPerformOperationNotification object:self userInfo:@{APESideViewsControllerNotificationOperationKey: @(operation), APESideViewsControllerNotificationViewControllerKey: sideViewController}];
            }];
        } else {
            *showingIvar = showing;
        }
    }
}
- (void)willPerformOperation:(APESideViewsControllerOperation)operation onViewController:(UIViewController *)viewController {
    if ([self.delegate respondsToSelector:@selector(sideViewsController:willPerformOperation:onViewController:)]) {
        [self.delegate sideViewsController:self willPerformOperation:operation onViewController:viewController];
    }
}
- (void)didPerformOperation:(APESideViewsControllerOperation)operation onViewController:(UIViewController *)viewController {
    if ([self.delegate respondsToSelector:@selector(sideViewsController:didPerformOperation:onViewController:)]) {
        [self.delegate sideViewsController:self didPerformOperation:operation onViewController:viewController];
    }
}

#pragma mark - Rotation
- (BOOL)shouldAutorotate {
    return (_currentOperation == APESideViewsControllerOperationNone);
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.layout invalidateLayout];
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self reloadLayout];
}

#pragma mark - Decoration views
- (UIView *)dequeueReusableDecorationViewForIdentifier:(NSString *)identifier {
    UIView *view = _decorationViews[identifier];
    if (!view) {
        Class viewClass = [_layout classForDecorationViewWithIdentifier:identifier];
        view = [[viewClass alloc] init];
    }
    return view;
}

#pragma mark - Statusbar style
- (UIViewController *)childViewControllerForStatusBarStyle {
    return self.layout.childViewControllerForStatusBarStyle;
}

#pragma mark - Gesture recognizer delegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (gestureRecognizer == self.mainTapRecognizer) {
        return (_showingLeftViewController || _showingRightViewController);
    }
    return YES;
}

#pragma mark - Private
- (void)removeViewController:(UIViewController *)viewController {
    [viewController willMoveToParentViewController:nil];
    [viewController.view removeFromSuperview];
    [viewController removeFromParentViewController];
}
- (void)performOperation:(APESideViewsControllerOperation)operation withViewController:(UIViewController *)viewController animated:(BOOL)animated completion:(VoidBlock)completion {
    
    if (self.operationsDisabled) {
        return;
    }
    
    [self willPerformOperation:operation onViewController:viewController];
    
    if (animated) {
        _currentOperation = operation;
        _operatingViewController = viewController;
        
        self.view.userInteractionEnabled = NO;
        
        APESideViewsConcreteContextTransitioning *context = [[APESideViewsConcreteContextTransitioning alloc] initWithSideViewsController:self operation:operation operatingViewController:viewController];
        context.initialDecorationViews = _decorationViews;
        context.transitionSide = (viewController == self.leftViewController) ? APESideViewsTransitionSideLeft : APESideViewsTransitionSideRight;
        
        if ([self.delegate respondsToSelector:@selector(sideViewsController:animationControllerForOperation:onViewController:)]) {
            context.animatedTransitioning = [self.delegate sideViewsController:self animationControllerForOperation:operation onViewController:viewController];
        } else {
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                context.animatedTransitioning = [APESideViewsViewBasedAnimation new];
            } else {
                context.animatedTransitioning = [APESideViewsOverlaySlideAnimation new];
            }
        }
        
        // Handle interaction controllers
        if ([self.defaultInteractionController isInteracting]) {
            context.interactive = YES;
            context.interactiveTransitioning = self.defaultInteractionController;
        } else if ([self.delegate respondsToSelector:@selector(sideViewsController:interactionControllerForOperation:onViewController:)]) {
            id<UIViewControllerInteractiveTransitioning> interactiveTransitioning = [self.delegate sideViewsController:self interactionControllerForOperation:operation onViewController:viewController];
            context.interactive = (interactiveTransitioning != nil);
            context.interactiveTransitioning = interactiveTransitioning;
        }
        
        __weak APESideViewsController *weakSelf = self;
        context.completion = ^(BOOL didComplete, NSDictionary *finalDecorationViews) {
            
            self.view.userInteractionEnabled = YES;
            _currentOperation = APESideViewsControllerOperationNone;
            _operatingViewController = nil;
            if (completion && didComplete) {
                completion();
            }
            
            _decorationViews = finalDecorationViews;
            // Update decoration views to where they should be
            [self reloadLayout];
            
            if (didComplete) {
                [weakSelf didPerformOperation:operation onViewController:viewController];
            }
        };
        
        [context perform];
    } else {
        // Non-animated
        [self reloadLayout];
        if (completion) {
            completion();
        }
    }
    
}
- (void)reloadLayout {
    [_layout invalidateLayout];
    
    [self.view addSubview:self.mainContainerView];
    [self insertViewController:_mainViewController inContainerView:self.mainContainerView withLayoutAttributes:[_layout layoutAttributesForViewController:_mainViewController]];
    
    if (_showingLeftViewController) {
        [self.view addSubview:self.leftContainerView];
        [self insertViewController:_leftViewController inContainerView:self.leftContainerView withLayoutAttributes:[_layout layoutAttributesForViewController:_leftViewController]];
    } else if (_leftViewController.isViewLoaded) {
        [self removeContainerView:self.leftContainerView];
    }
    
    if (_showingRightViewController) {
        [self.view addSubview:self.rightContainerView];
        [self insertViewController:_rightViewController inContainerView:self.rightContainerView withLayoutAttributes:[_layout layoutAttributesForViewController:_rightViewController]];
    } else if (_rightViewController.isViewLoaded) {
        [self removeContainerView:self.rightContainerView];
    }
    
    [self reloadDecorationViews];
    [self updateUserInteractionStates];
    [self setNeedsStatusBarAppearanceUpdate];
}
- (void)insertViewController:(UIViewController *)viewController inContainerView:(UIView *)containerView withLayoutAttributes:(APESideViewsLayoutAttributes *)attributes {
    [containerView setLayoutAttributes:attributes];
    [viewController.view setLayoutAttributes:[[APESideViewsLayoutAttributes alloc] initWithFrame:containerView.bounds]]; // Make sure the view controller doesn't have any transformations or something on it
    viewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    if (![containerView.subviews containsObject:viewController.view] && viewController.presentedViewController.modalPresentationStyle != UIModalPresentationCurrentContext) {
        [containerView addSubview:viewController.view];
    }
}
- (void)removeContainerView:(UIView *)containerView {
    for (UIView *view in containerView.subviews) {
        [view removeFromSuperview];
    }
    [containerView removeFromSuperview];
}
- (void)updateUserInteractionStates {
    self.mainViewController.view.userInteractionEnabled = !((_showingLeftViewController || _showingRightViewController) && _disablesMainInteractionWhenShowingSideView);
}
- (void)reloadDecorationViews {
    for (UIView *view in [_decorationViews allValues]) {
        [view removeFromSuperview];
    }
    
    NSMutableDictionary *newDecorationViews = [NSMutableDictionary dictionary];
    for (APESideViewsLayoutAttributes *attributes in [[_layout layoutAttributesForDecorationViews] allValues]) {
        UIView *view = [self dequeueReusableDecorationViewForIdentifier:attributes.decorationViewIdentifier];
        if (view) {
            [self.view addSubview:view];
            [view setLayoutAttributes:attributes];
            newDecorationViews[attributes.decorationViewIdentifier] = view;
        }
    }
    _decorationViews = newDecorationViews;
}
- (UIView *)newContainerView {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    return view;
}
- (void)setupGestureRecognizers {
    
    self.mainTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mainViewTapped:)];
    self.mainTapRecognizer.delegate = self;
    [self.mainContainerView addGestureRecognizer:self.mainTapRecognizer];

    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.defaultInteractionController = [[APESideViewsPanInteractionController alloc] initWithSideViewsController:self];
        [self.mainContainerView addGestureRecognizer:self.interactiveGestureRecognizer];
        [self.mainTapRecognizer requireGestureRecognizerToFail:self.defaultInteractionController.gestureRecognizer];
    } else {
        UISwipeGestureRecognizer *leftSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(mainViewSwiped:)];
        leftSwipeRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
        [self.mainContainerView addGestureRecognizer:leftSwipeRecognizer];
        [self.mainTapRecognizer requireGestureRecognizerToFail:leftSwipeRecognizer];
        
        UISwipeGestureRecognizer *rightSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(mainViewSwiped:)];
        rightSwipeRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
        [self.mainContainerView addGestureRecognizer:rightSwipeRecognizer];
        [self.mainTapRecognizer requireGestureRecognizerToFail:rightSwipeRecognizer];
    }
    
}

@end


#pragma mark - UIViewController category
@implementation UIViewController (APESideViewsController)
- (APESideViewsController *)sideViewsController {
    
    APESideViewsController *sideViewsController = nil;
    
    // Try and find side views controller in VC-hierarchy
    UIViewController *child;
    UIViewController *parent = self.parentViewController;
    
    do {
        if ([parent isKindOfClass:[APESideViewsController class]]) {
            sideViewsController = (APESideViewsController *)parent;
        }
        
        child = parent;
        parent = child.parentViewController;
        
    } while (parent && !sideViewsController);
    
    return sideViewsController;
}
@end