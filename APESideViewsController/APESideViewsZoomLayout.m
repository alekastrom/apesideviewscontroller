//
//  APESideViewsZoomLayout.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-28.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsZoomLayout.h"

const CGFloat MainViewScale = 0.4;
const CGFloat MainViewTranslationXMultiplier = 0.25;
const CGFloat MainViewTranslationYMultiplier = 0.15;

@interface APESideViewsZoomAnimation : APESideViewsViewBasedAnimation @end
@implementation APESideViewsZoomLayout

- (id)initWithSideViewsController:(APESideViewsController *)sideViewsController {
    self = [super initWithSideViewsController:sideViewsController];
    if (self) {
        self.useStatusBarStyleFromPresentedSideViewController = YES;
    }
    return self;
}

- (APESideViewsLayoutAttributes *)mainAttributesWithOpenSide:(APESlideOpenSide)openSide {
    
    APESideViewsMutableLayoutAttributes *attributes = [[super mainAttributesWithOpenSide:APESlideOpenSideNone] mutableCopy];
    
    if (openSide != APESlideOpenSideNone) {
        BOOL leftOpen = (openSide == APESlideOpenSideLeft);
        attributes.transform3D = CATransform3DMakeAffineTransform([[self class] mainViewTransformWithContainerBounds:self.sideViewsController.view.bounds translateToRight:leftOpen]);
    }
    
    return attributes;
}

+ (APESideViewsViewBasedAnimation *)zoomAnimation {
    return [APESideViewsZoomAnimation new];
}

+ (CGAffineTransform)mainViewTransformWithContainerBounds:(CGRect)bounds translateToRight:(BOOL)toRight {
    CGAffineTransform t = CGAffineTransformIdentity;
    t = CGAffineTransformTranslate(t, (toRight?1:-1)*MainViewTranslationXMultiplier*bounds.size.width, MainViewTranslationYMultiplier*bounds.size.height);
    t = CGAffineTransformScale(t, MainViewScale, MainViewScale);
    return t;
}

- (NSDictionary *)layoutAttributesForDecorationViews {
    return nil;
}
- (NSDictionary *)finalLayoutAttributesForDecorationViews {
    return nil;
}

@end

@implementation APESideViewsZoomAnimation

- (id)init {
    self = [super init];
    if (self) {
        self.showDampening = 0.9;
    }
    return self;
}

- (APESideViewsLayoutAttributes *)beginAnimationAttributesForViewController:(UIViewController *)viewController initialAttributes:(APESideViewsLayoutAttributes *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes {
    if (initialAttributes) {
        return initialAttributes;
    } else if (viewController == [self.transitionContext viewControllerForKey:APESideViewsTransitionContextOperatingViewControllerKey]) {
        return [self zoomedOutAttributesWithOriginals:finalAttributes];
    } else {
        return finalAttributes;
    }
}
- (APESideViewsLayoutAttributes *)endAnimationAttributesForViewController:(UIViewController *)viewController initialAttributes:(APESideViewsLayoutAttributes *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes {
    if (finalAttributes) {
        return finalAttributes;
    } else if (viewController == [self.transitionContext viewControllerForKey:APESideViewsTransitionContextOperatingViewControllerKey]) {
        return [self zoomedOutAttributesWithOriginals:initialAttributes];
    } else {
        return finalAttributes;
    }
}

- (APESideViewsLayoutAttributes *)zoomedOutAttributesWithOriginals:(APESideViewsLayoutAttributes *)originalAttributes {
    APESideViewsMutableLayoutAttributes *attributes = [originalAttributes mutableCopy];
    
    attributes.transform3D = CATransform3DMakeAffineTransform(CGAffineTransformInvert([APESideViewsZoomLayout mainViewTransformWithContainerBounds:[self.transitionContext containerView].bounds translateToRight:([self.transitionContext transitionSide] == APESideViewsTransitionSideLeft)]));

    return attributes;
}

@end
