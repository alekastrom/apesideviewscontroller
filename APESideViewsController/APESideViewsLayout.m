//
//  APESideViewsLayout.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-18.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsLayout.h"

@implementation APESideViewsLayout {
    APESideViewsControllerOperation _currentOperation;
    __weak UIViewController *_operatingViewController;
    NSMutableDictionary *_decorationClasses;
}

- (instancetype)initWithSideViewsController:(APESideViewsController *)sideViewsController {
    self = [super init];
    if (self) {
        _sideViewsController = sideViewsController;
        _decorationClasses = [NSMutableDictionary dictionary];
        _useStatusBarStyleFromPresentedSideViewController = NO;
        [self prepareLayout];
    }
    return self;
}

- (void)prepareLayout {
}
- (void)invalidateLayout {
    [self prepareLayout];
}

- (void)prepareForOperation:(APESideViewsControllerOperation)operation onViewController:(UIViewController *)viewController {
    _currentOperation = operation;
    _operatingViewController = viewController;
}

- (APESideViewsLayoutAttributes *)layoutAttributesForViewController:(UIViewController *)viewController {
    NSAssert(NO, @"Layout attributes must be overridden");
    return nil;
}
- (APESideViewsLayoutAttributes *)finalLayoutAttributesForViewController:(UIViewController *)viewController {
    NSAssert(NO, @"Final layout attributes must be overridden");
    return nil;
}

- (void)setUseStatusBarStyleFromPresentedSideViewController:(BOOL)useStatusBarStyleFromPresentedSideViewController {
    if (_useStatusBarStyleFromPresentedSideViewController != useStatusBarStyleFromPresentedSideViewController) {
        _useStatusBarStyleFromPresentedSideViewController = useStatusBarStyleFromPresentedSideViewController;
        [_sideViewsController setNeedsStatusBarAppearanceUpdate];
    }
}

- (void)finalizeOperation {
    _currentOperation = APESideViewsControllerOperationNone;
    _operatingViewController = nil;
}

- (NSDictionary *)layoutAttributesForDecorationViews {
    return nil;
}
- (NSDictionary *)finalLayoutAttributesForDecorationViews {
    return nil;
}

- (void)registerClass:(Class)class forDecorationViewWithIdentifier:(NSString *)decorationViewIdentifier {
    if (class == NULL && _decorationClasses[decorationViewIdentifier]) {
        [_decorationClasses removeObjectForKey:decorationViewIdentifier];
    } else if (class) {
        _decorationClasses[decorationViewIdentifier] = class;
    }
}

- (UIViewController *)childViewControllerForStatusBarStyle {
    if (self.useStatusBarStyleFromPresentedSideViewController) {
        if ((_sideViewsController.isShowingLeftViewController && _currentOperation != APESideViewsControllerOperationHideSideView) || (_currentOperation == APESideViewsControllerOperationShowSideView && _operatingViewController == _sideViewsController.leftViewController)) {
            return _sideViewsController.leftViewController;
        } else if ((_sideViewsController.isShowingRightViewController && _currentOperation != APESideViewsControllerOperationHideSideView) || (_currentOperation == APESideViewsControllerOperationShowSideView && _operatingViewController == _sideViewsController.rightViewController)) {
            return _sideViewsController.rightViewController;
        }
    }
    
    return _sideViewsController.mainViewController;
}

@end

@implementation APESideViewsLayout (APESideViewsControllerAdditions)

- (Class)classForDecorationViewWithIdentifier:(NSString *)decorationViewIdentifier {
    return _decorationClasses[decorationViewIdentifier];
}

@end
