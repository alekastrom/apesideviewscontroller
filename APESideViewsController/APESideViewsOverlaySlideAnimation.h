//
//  APESideViewsOverlaySlideAnimation.h
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-26.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsViewBasedAnimation.h"

@interface APESideViewsOverlaySlideAnimation : APESideViewsViewBasedAnimation

@end
