//
//  APESideViewsConcreteContextTransitioning.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-25.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsConcreteContextTransitioning.h"
#import "APESideViewsFadeAnimation.h"

NSString *const APESideViewsTransitionContextMainViewControllerKey = @"APESideViewsTransitionContextMainViewController";
NSString *const APESideViewsTransitionContextOperatingViewControllerKey = @"APESideViewsTransitionContextOperatingViewController";

@implementation APESideViewsConcreteContextTransitioning {
    APESideViewsController *_sideViewsController;
    UIView *_containerView;
    UIViewController *_operatingViewController;
    NSDictionary *_decorationViewsDuringTransitioning;
    NSDictionary *_finalDecorationViews;
    
    BOOL _transitionEnded;
}

#pragma mark - Initialization
- (instancetype)initWithSideViewsController:(APESideViewsController *)sideViewsController operation:(APESideViewsControllerOperation)operation operatingViewController:(UIViewController *)operatingViewController {
    self = [super init];
    if (self) {
        _sideViewsController = sideViewsController;
        _operation = operation;
        _operatingViewController = operatingViewController;
    }
    return self;
}

#pragma mark - Accessors
- (UIView *)containerView {
    return _containerView;
}
- (BOOL)isAnimated {
    return YES;
}
- (UIModalPresentationStyle)presentationStyle {
    return UIModalPresentationNone;
}
- (UIViewController *)viewControllerForKey:(NSString *)key {
    if ([key isEqualToString:UITransitionContextFromViewControllerKey]) {
        if (_operation == APESideViewsControllerOperationShowSideView) {
            return _sideViewsController.mainViewController;
        } else {
            return _operatingViewController;
        }
    } else if ([key isEqualToString:UITransitionContextToViewControllerKey]) {
        if (_operation == APESideViewsControllerOperationShowSideView) {
            return _operatingViewController;
        } else {
            return _sideViewsController.mainViewController;
        }
    } else if ([key isEqualToString:APESideViewsTransitionContextMainViewControllerKey]) {
        return _sideViewsController.mainViewController;
    } else if ([key isEqualToString:APESideViewsTransitionContextOperatingViewControllerKey]) {
        return _operatingViewController;
    } else {
        return nil;
    }
}
- (NSDictionary *)decorationViews {
    return _decorationViewsDuringTransitioning;
}
- (id<APESideViewsAnimatedTransitioning>)animator {
    return self.animatedTransitioning;
}

#pragma mark Layouting
- (APESideViewsLayoutAttributes *)initialAttributesForViewController:(UIViewController *)viewController {
    return [_sideViewsController.layout layoutAttributesForViewController:viewController];
}
- (APESideViewsLayoutAttributes *)finalAttributesForViewController:(UIViewController *)viewController {
    return [_sideViewsController.layout finalLayoutAttributesForViewController:viewController];
}
- (CGRect)initialFrameForViewController:(UIViewController *)vc {
    APESideViewsLayoutAttributes *attributes = [self initialAttributesForViewController:vc];
    if (attributes) {
        return attributes.frame;
    } else {
        return CGRectZero;
    }
}
- (CGRect)finalFrameForViewController:(UIViewController *)vc {
    APESideViewsLayoutAttributes *attributes = [self finalAttributesForViewController:vc];
    if (attributes) {
        return attributes.frame;
    } else {
        return CGRectZero;
    }
}
- (NSDictionary *)initialAttributesForDecorationViews {
    return [_sideViewsController.layout layoutAttributesForDecorationViews];
}
- (NSDictionary *)finalAttributesForDecorationViews {
    return [_sideViewsController.layout finalLayoutAttributesForDecorationViews];
}

#pragma mark - Transitioning
- (void)perform {
    [_sideViewsController.layout prepareForOperation:_operation onViewController:_operatingViewController];
    _decorationViewsDuringTransitioning = [self decorationViewsDuringOperation];

    _containerView = [[UIView alloc] initWithFrame:_sideViewsController.view.bounds];
    _containerView.backgroundColor = [UIColor clearColor];
    [_sideViewsController.view addSubview:_containerView];
    [self moveViewControllersToContainer];
    [self moveDecorationViewsToContainer];
    
    if (_interactiveTransitioning) {
        [_interactiveTransitioning startInteractiveTransition:self];
    } else {
        [_animatedTransitioning animateSideViewsTransition:self];
    }
    
    [UIView animateWithDuration:[_animatedTransitioning transitionDuration:self] delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [_sideViewsController setNeedsStatusBarAppearanceUpdate];
    } completion:^(BOOL finished) {
    }];
}
- (void)completeTransition:(BOOL)didComplete {
    
    if (_transitionEnded) {
        return;
    }
    _transitionEnded = YES;
    
    // Clean up decoration views
    NSMutableDictionary *finalDecorationViews = [NSMutableDictionary dictionary];
    [_decorationViewsDuringTransitioning enumerateKeysAndObjectsUsingBlock:^(NSString *identifier, UIView *view, BOOL *stop) {
        if ([self finalAttributesForDecorationViews][identifier]) {
            finalDecorationViews[identifier] = view;
        }
        
        [view removeFromSuperview];
    }];
    _finalDecorationViews = finalDecorationViews;
    
    [_sideViewsController.layout finalizeOperation];
    [_animatedTransitioning animationEnded:didComplete];
    if (self.completion) {
        self.completion(didComplete, _finalDecorationViews);
    }
    // Remove container view and it's subviews
    [_containerView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [_containerView removeFromSuperview];
}

#pragma mark - Interaction
- (void)updateInteractiveTransition:(CGFloat)percentComplete {
}
- (void)finishInteractiveTransition {
}
- (void)cancelInteractiveTransition {
}

#pragma mark - Private
- (void)moveViewControllersToContainer {
    APESideViewsLayout *layout = _sideViewsController.layout;
    UIViewController *mainVC = _sideViewsController.mainViewController;
    [_containerView addSubview:mainVC.view];
    [mainVC.view setLayoutAttributes:[layout layoutAttributesForViewController:mainVC]];
    
    UIViewController *leftVC = _sideViewsController.leftViewController;
    APESideViewsLayoutAttributes *leftAttributes = [layout layoutAttributesForViewController:leftVC];
    if (leftAttributes) {
        [_containerView addSubview:leftVC.view];
        [leftVC.view setLayoutAttributes:leftAttributes];
    }
    
    UIViewController *rightVC = _sideViewsController.rightViewController;
    APESideViewsLayoutAttributes *rightAttributes = [layout layoutAttributesForViewController:rightVC];
    if (rightAttributes) {
        [_containerView addSubview:rightVC.view];
        [rightVC.view setLayoutAttributes:rightAttributes];
    }
}
- (void)moveDecorationViewsToContainer {
    NSDictionary *attributes = [self initialAttributesForDecorationViews];
    [_initialDecorationViews enumerateKeysAndObjectsUsingBlock:^(NSString *identifier, UIView *view, BOOL *stop) {
        [self.containerView addSubview:view];
        [view setLayoutAttributes:attributes[identifier]];
    }];
}
- (NSDictionary *)decorationViewsDuringOperation {
    NSMutableDictionary *decorationViews = [_initialDecorationViews mutableCopy];
    [[_sideViewsController.layout finalLayoutAttributesForDecorationViews] enumerateKeysAndObjectsUsingBlock:^(NSString *identifier, APESideViewsLayoutAttributes *attributes, BOOL *stop) {
        if (!decorationViews[identifier]) {
            UIView *view = [_sideViewsController dequeueReusableDecorationViewForIdentifier:attributes.decorationViewIdentifier];
            if (view) {
                decorationViews[identifier] = view;
            }
        }
    }];
    return decorationViews;
}

@end
