//
//  APESideViewsLayoutAttributes.h
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-19.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

@import Foundation;

@interface APESideViewsLayoutAttributes : NSObject <NSCopying, NSMutableCopying>
- (instancetype)initWithFrame:(CGRect)frame;
- (instancetype)initWithFrame:(CGRect)frame transform3d:(CATransform3D)transform3d zIndex:(NSInteger)zIndex alpha:(CGFloat)alpha decorationViewIdentifier:(NSString *)decorationViewIdentifier;

@property (nonatomic, readonly) CGRect frame;
@property (nonatomic, readonly) CATransform3D transform3D;
@property (nonatomic, readonly) NSInteger zIndex; // default is 0
@property (nonatomic, readonly) CGFloat alpha;
@property (nonatomic, copy, readonly) NSString *decorationViewIdentifier;
@property (nonatomic, readonly) BOOL isHidden;
@end


@interface APESideViewsMutableLayoutAttributes : APESideViewsLayoutAttributes
@property (nonatomic) CGRect frame;
@property (nonatomic) CATransform3D transform3D;
@property (nonatomic) NSInteger zIndex; // default is 0
@property (nonatomic) CGFloat alpha;
@property (nonatomic, copy) NSString *decorationViewIdentifier;
@end


@interface UIView (APESideViewsLayoutAttributes)
- (void)setLayoutAttributes:(APESideViewsLayoutAttributes *)attributes;
@end
