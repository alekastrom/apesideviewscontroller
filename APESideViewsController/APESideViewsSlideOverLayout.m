//
//  APESideViewsSlideOverLayout.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-27.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsSlideOverLayout.h"
#import "APEHairlineView.h"

NSString *const APESlideOverLayoutLeftEdgeDecoration = @"APESlideOverLayoutLeftEdgeDecoration";
NSString *const APESlideOverLayoutRightEdgeDecoration = @"APESlideOverLayoutLeftEdgeDecoration";
NSString *const APESlideOverLayoutStatusBarDecoration = @"APESlideOverLayoutStatusBarDecoration";

@implementation APESideViewsSlideOverLayout

- (void)prepareLayout {
    [super prepareLayout];
    
    [self registerClass:[APEHairlineView class] forDecorationViewWithIdentifier:APESlideOverLayoutLeftEdgeDecoration];
    [self registerClass:[APEHairlineView class] forDecorationViewWithIdentifier:APESlideOverLayoutRightEdgeDecoration];
}

#pragma mark - View controller layout
- (APESideViewsLayoutAttributes *)layoutAttributesForViewController:(UIViewController *)viewController {
    
    if (viewController == self.sideViewsController.leftViewController || viewController == self.sideViewsController.rightViewController) {
        return [self sideViewControllerAttributesWhenShowing:[self.sideViewsController isShowingViewController:viewController]];
    } else if (self.sideViewsController.isShowingLeftViewController) {
        return [self mainAttributesWithOpenSide:APESlideOpenSideLeft];
    } else if (self.sideViewsController.isShowingRightViewController) {
        return [self mainAttributesWithOpenSide:APESlideOpenSideRight];
    } else {
        return [self mainAttributesWithOpenSide:APESlideOpenSideNone];
    }
}
- (APESideViewsLayoutAttributes *)finalLayoutAttributesForViewController:(UIViewController *)viewController {
    if (viewController == self.sideViewsController.leftViewController || viewController == self.sideViewsController.rightViewController) {
        return [self sideViewControllerAttributesWhenShowing:(self.currentOperation == APESideViewsControllerOperationShowSideView)];
    } else {
        APESlideOpenSide openSide = APESlideOpenSideNone;
        if (self.currentOperation == APESideViewsControllerOperationShowSideView) {
            openSide = (self.operatingViewController == self.sideViewsController.leftViewController) ? APESlideOpenSideLeft : APESlideOpenSideRight;
        }
        
        return [self mainAttributesWithOpenSide:openSide];
    }
}
- (NSDictionary *)layoutAttributesForDecorationViews {
    NSMutableDictionary *attributes = [[self attributesForMainEdgesMainAttributes:[self layoutAttributesForViewController:self.sideViewsController.mainViewController]] mutableCopy];
    attributes[APESlideOverLayoutStatusBarDecoration] = [self statusBarOverlayAttributesVisible:(self.sideViewsController.isShowingLeftViewController || self.sideViewsController.isShowingRightViewController)];
    return attributes;
}
- (NSDictionary *)finalLayoutAttributesForDecorationViews {
    NSMutableDictionary *attributes = [[self attributesForMainEdgesMainAttributes:[self finalLayoutAttributesForViewController:self.sideViewsController.mainViewController]] mutableCopy];
    attributes[APESlideOverLayoutStatusBarDecoration] = [self statusBarOverlayAttributesVisible:(self.currentOperation == APESideViewsControllerOperationShowSideView)];
    return attributes;
}

#pragma mark - Private
- (APESideViewsLayoutAttributes *)sideViewControllerAttributesWhenShowing:(BOOL)showing {
    if (showing) {
        return [[APESideViewsLayoutAttributes alloc] initWithFrame:self.sideViewsController.view.bounds transform3d:CATransform3DIdentity zIndex:0 alpha:1 decorationViewIdentifier:nil];
    } else {
        return nil;
    }
}
- (APESideViewsLayoutAttributes *)mainAttributesWithOpenSide:(APESlideOpenSide)openSide {
    
    CGRect frame = self.sideViewsController.view.bounds;
    
    if (openSide == APESlideOpenSideLeft) {
        frame.origin.x = self.sideViewsController.leftViewController.preferredContentSize.width;
    } else if (openSide == APESlideOpenSideRight) {
        frame.origin.x = -self.sideViewsController.rightViewController.preferredContentSize.width;
    }
    
    return [[APESideViewsLayoutAttributes alloc] initWithFrame:frame transform3d:CATransform3DIdentity zIndex:1 alpha:1 decorationViewIdentifier:nil];
}
- (NSDictionary *)attributesForMainEdgesMainAttributes:(APESideViewsLayoutAttributes *)attributes {
    
    APESideViewsMutableLayoutAttributes *newAttributes = [attributes mutableCopy];
    newAttributes.zIndex = 3;
    
    CGRect leftFrame = attributes.frame;
    CGRect rightFrame = attributes.frame;
    CGFloat hairLineWidth = 1.0/[[UIScreen mainScreen] scale];
    
    leftFrame.origin.x -= hairLineWidth; // Offset the hairline to the left of the view controller
    rightFrame.origin.x += rightFrame.size.width; // Offset the hairline to the right of the view controller
    
    leftFrame.size.width = rightFrame.size.width = hairLineWidth;

    APESideViewsMutableLayoutAttributes *leftAttributes = [newAttributes mutableCopy];
    leftAttributes.frame = leftFrame;
    leftAttributes.decorationViewIdentifier = APESlideOverLayoutLeftEdgeDecoration;
    
    APESideViewsMutableLayoutAttributes *rightAttributes = newAttributes;
    rightAttributes.frame = rightFrame;
    rightAttributes.decorationViewIdentifier = APESlideOverLayoutRightEdgeDecoration;
    
    return @{APESlideOverLayoutLeftEdgeDecoration : leftAttributes, APESlideOverLayoutRightEdgeDecoration : rightAttributes};
}
- (APESideViewsLayoutAttributes *)statusBarOverlayAttributesVisible:(BOOL)visible {
    return [[APESideViewsLayoutAttributes alloc] initWithFrame:CGRectMake(0, 0, self.sideViewsController.view.bounds.size.width, 20.0f) transform3d:CATransform3DIdentity zIndex:3 alpha:visible?1:0 decorationViewIdentifier:APESlideOverLayoutStatusBarDecoration];
}

@end
