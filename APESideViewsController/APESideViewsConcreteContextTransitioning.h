//
//  APESideViewsConcreteContextTransitioning.h
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-25.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

@import UIKit;
#import "APESideViewsController.h"
#import "APESideViewsLayout.h"

@interface APESideViewsController (DecorationViews)
- (UIView *)dequeueReusableDecorationViewForIdentifier:(NSString *)identifier;
@end

typedef void(^ContextTransitioningBlock)(BOOL didComplete, NSDictionary *finalDecorationViews);
@interface APESideViewsConcreteContextTransitioning : NSObject <APESideViewsContextTransitioning>

- (instancetype)initWithSideViewsController:(APESideViewsController *)sideViewsController operation:(APESideViewsControllerOperation)operation operatingViewController:(UIViewController *)operatingViewController;

@property (nonatomic, getter = isInteractive) BOOL interactive;
@property (nonatomic) BOOL transitionWasCancelled;
@property (nonatomic) APESideViewsTransitionSide transitionSide;
@property (nonatomic) APESideViewsControllerOperation operation;
@property (nonatomic, copy) NSDictionary *initialDecorationViews;
@property (nonatomic, copy) ContextTransitioningBlock completion;
@property (nonatomic, strong) id<APESideViewsAnimatedTransitioning> animatedTransitioning;
@property (nonatomic, strong) id<UIViewControllerInteractiveTransitioning> interactiveTransitioning;

- (void)perform;

@end
