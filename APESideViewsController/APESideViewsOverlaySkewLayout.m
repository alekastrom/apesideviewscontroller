//
//  APESideViewsOverlaySkewLayout.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-29.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsOverlaySkewLayout.h"

@implementation APESideViewsOverlaySkewLayout

#define CATransform3DPerspective(t, x, y) (CATransform3DConcat(t, CATransform3DMake(1, 0, 0, x, 0, 1, 0, y, 0, 0, 1, 0, 0, 0, 0, 1)))
#define CATransform3DMakePerspective(x, y) (CATransform3DPerspective(CATransform3DIdentity, x, y))

CG_INLINE CATransform3D
CATransform3DMake(CGFloat m11, CGFloat m12, CGFloat m13, CGFloat m14,
				  CGFloat m21, CGFloat m22, CGFloat m23, CGFloat m24,
				  CGFloat m31, CGFloat m32, CGFloat m33, CGFloat m34,
				  CGFloat m41, CGFloat m42, CGFloat m43, CGFloat m44)
{
	CATransform3D t;
	t.m11 = m11; t.m12 = m12; t.m13 = m13; t.m14 = m14;
	t.m21 = m21; t.m22 = m22; t.m23 = m23; t.m24 = m24;
	t.m31 = m31; t.m32 = m32; t.m33 = m33; t.m34 = m34;
	t.m41 = m41; t.m42 = m42; t.m43 = m43; t.m44 = m44;
	return t;
}

- (APESideViewsLayoutAttributes *)layoutAttributesForViewController:(UIViewController *)viewController {
    if (viewController == self.sideViewsController.mainViewController) {
        if (self.sideViewsController.isShowingLeftViewController) {
            return [self mainViewAttributesSkewedLeft:YES];
        } else if (self.sideViewsController.isShowingRightViewController) {
            return [self mainViewAttributesSkewedLeft:NO];
        }
    }
    
    return [super layoutAttributesForViewController:viewController];
}

- (APESideViewsLayoutAttributes *)finalLayoutAttributesForViewController:(UIViewController *)viewController {
    if (viewController == self.sideViewsController.mainViewController && self.currentOperation == APESideViewsControllerOperationShowSideView) {
        return [self mainViewAttributesSkewedLeft:(self.operatingViewController == self.sideViewsController.leftViewController)];
    } else {
        return [super finalLayoutAttributesForViewController:viewController];
    }
}

- (APESideViewsLayoutAttributes *)mainViewAttributesSkewedLeft:(BOOL)left {
    CATransform3D t = CATransform3DMakePerspective((left?-1:1)*0.0005, 0);
    t = CATransform3DScale(t, 0.8, 0.8, 1);
    return [[APESideViewsLayoutAttributes alloc] initWithFrame:self.sideViewsController.view.bounds transform3d:t zIndex:0 alpha:1 decorationViewIdentifier:nil];
}

@end
