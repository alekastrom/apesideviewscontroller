//
//  APESideViewsViewBasedAnimation.h
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-26.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsController.h"

@interface APESideViewsViewBasedAnimation : NSObject <APESideViewsAnimatedTransitioning>

// Hooks
- (APESideViewsLayoutAttributes *)beginAnimationAttributesForViewController:(UIViewController *)viewController initialAttributes:(APESideViewsLayoutAttributes  *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes;
- (APESideViewsLayoutAttributes *)endAnimationAttributesForViewController:(UIViewController *)viewController initialAttributes:(APESideViewsLayoutAttributes  *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes;

- (APESideViewsLayoutAttributes *)beginAnimationAttributesForDecorationView:(UIView *)decorationView identifier:(NSString *)identifier initialAttributes:(APESideViewsLayoutAttributes  *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes;

- (APESideViewsLayoutAttributes *)endAnimationAttributesForDecorationView:(UIView *)decorationView identifier:(NSString *)identifier initialAttributes:(APESideViewsLayoutAttributes  *)initialAttributes finalAttributes:(APESideViewsLayoutAttributes *)finalAttributes;

@property (nonatomic, weak) id<APESideViewsContextTransitioning> transitionContext;
@property (nonatomic) NSTimeInterval duration;
@property (nonatomic) CGFloat showDampening;

@end
