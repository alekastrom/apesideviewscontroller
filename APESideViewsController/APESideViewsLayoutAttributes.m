//
//  APESideViewsLayoutAttributes.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-19.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsLayoutAttributes.h"

@implementation APESideViewsLayoutAttributes

- (instancetype)initWithFrame:(CGRect)frame {
    return [self initWithFrame:frame transform3d:CATransform3DIdentity zIndex:0 alpha:1.0 decorationViewIdentifier:nil];
}
- (instancetype)initWithFrame:(CGRect)frame transform3d:(CATransform3D)transform3d zIndex:(NSInteger)zIndex alpha:(CGFloat)alpha decorationViewIdentifier:(NSString *)decorationViewIdentifier {
    self = [super init];
    if (self) {
        _frame = frame;
        _alpha = alpha;
        _transform3D = transform3d;
        _zIndex = zIndex;
        _decorationViewIdentifier = [decorationViewIdentifier copy];
    }
    return self;
}

- (BOOL)isHidden {
    return CGRectEqualToRect(self.frame, CGRectZero);
}

#pragma mark - Copying
- (id)copyWithZone:(NSZone *)zone {
    return self; // Common with immutable objects
}
- (id)mutableCopyWithZone:(NSZone *)zone {
    APESideViewsMutableLayoutAttributes *newAttributes = [[APESideViewsMutableLayoutAttributes allocWithZone:zone] init];
    newAttributes.frame = _frame;
    newAttributes.transform3D = _transform3D;
    newAttributes.zIndex = _zIndex;
    newAttributes.alpha = _alpha;
    newAttributes.decorationViewIdentifier = _decorationViewIdentifier;
    return newAttributes;
}

@end

@implementation APESideViewsMutableLayoutAttributes
@synthesize decorationViewIdentifier;
@synthesize frame;
@synthesize alpha;
@synthesize transform3D;
@synthesize zIndex;

- (id)mutableCopyWithZone:(NSZone *)zone {
    APESideViewsMutableLayoutAttributes *newAttributes = [[APESideViewsMutableLayoutAttributes allocWithZone:zone] init];
    newAttributes.frame = self.frame;
    newAttributes.transform3D = self.transform3D;
    newAttributes.zIndex = self.zIndex;
    newAttributes.alpha = self.alpha;
    newAttributes.decorationViewIdentifier = self.decorationViewIdentifier;
    return newAttributes;
}
@end

@implementation UIView (APESideViewsLayoutAttributes)

- (void)setLayoutAttributes:(APESideViewsLayoutAttributes *)attributes {
    // Always set transform to identity before changing frame
    self.layer.transform = CATransform3DIdentity;
    self.frame = attributes.frame;
    self.alpha = attributes.alpha;
    self.layer.transform = attributes.transform3D;
    [self.superview insertSubview:self atIndex:attributes.zIndex];
}

@end
