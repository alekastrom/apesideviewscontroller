//
//  APESideViewsOverlayLayout.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-19.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsOverlayLayout.h"
#import "APEHairlineView.h"

NSString *const APEOverlayLayoutLeftHairlineDecoration = @"APEOverlayLayoutLeftHairlineDecoration";
NSString *const APEOverlayLayoutRightHairlineDecoration = @"APEOverlayLayoutRightHairlineDecoration";

@implementation APESideViewsOverlayLayout
- (void)prepareLayout {
    [super prepareLayout];
    
    [self registerClass:[APEHairlineView class] forDecorationViewWithIdentifier:APEOverlayLayoutLeftHairlineDecoration];
    [self registerClass:[APEHairlineView class] forDecorationViewWithIdentifier:APEOverlayLayoutRightHairlineDecoration];
}

- (CGSize)mainSize {
    return self.sideViewsController.view.bounds.size;
}

#pragma mark - View controller layout
- (APESideViewsLayoutAttributes *)layoutAttributesForViewController:(UIViewController *)viewController {
    if (viewController == self.sideViewsController.mainViewController) {
        return [self mainViewControllerAttributes];
    } else if (viewController == self.sideViewsController.leftViewController) {
        return [self attributesForSideViewController:viewController showing:self.sideViewsController.showingLeftViewController];
    } else if (viewController == self.sideViewsController.rightViewController) {
        return [self attributesForSideViewController:viewController showing:self.sideViewsController.showingRightViewController];
    } else {
        return nil;
    }
}
- (APESideViewsLayoutAttributes *)finalLayoutAttributesForViewController:(UIViewController *)viewController {
    if (viewController == self.sideViewsController.mainViewController) {
        return [self mainViewControllerAttributes];
    } else {
        BOOL showing = NO;
        if (self.currentOperation == APESideViewsControllerOperationShowSideView && viewController == self.operatingViewController) {
            showing = YES;
        } else if (viewController != self.operatingViewController && [self.sideViewsController isShowingViewController:viewController]) {
            showing = YES;
        }
        return [self attributesForSideViewController:viewController showing:showing];
    }
}
- (APESideViewsLayoutAttributes *)mainViewControllerAttributes {
    return [[APESideViewsLayoutAttributes alloc] initWithFrame:self.sideViewsController.view.bounds];
}

#pragma mark - Decoration views
- (NSDictionary *)layoutAttributesForDecorationViews {
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    
    if (self.sideViewsController.leftViewController) {
        APESideViewsLayoutAttributes *leftAttributes = [self attributesForHairlineForViewController:self.sideViewsController.leftViewController withAttributes:[self layoutAttributesForViewController:self.sideViewsController.leftViewController]];
        if (leftAttributes) attributes[leftAttributes.decorationViewIdentifier] = leftAttributes;
    }
    
    if (self.sideViewsController.rightViewController) {
        APESideViewsLayoutAttributes *rightAttributes = [self attributesForHairlineForViewController:self.sideViewsController.rightViewController withAttributes:[self layoutAttributesForViewController:self.sideViewsController.rightViewController]];
        if (rightAttributes) attributes[rightAttributes.decorationViewIdentifier] = rightAttributes;
    }
    
    return attributes;
}
- (NSDictionary *)finalLayoutAttributesForDecorationViews {
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    
    if (self.sideViewsController.leftViewController) {
        APESideViewsLayoutAttributes *leftAttributes = [self attributesForHairlineForViewController:self.sideViewsController.leftViewController withAttributes:[self finalLayoutAttributesForViewController:self.sideViewsController.leftViewController]];
        if (leftAttributes) attributes[leftAttributes.decorationViewIdentifier] = leftAttributes;
    }
    
    if (self.sideViewsController.rightViewController) {
        APESideViewsLayoutAttributes *rightAttributes = [self attributesForHairlineForViewController:self.sideViewsController.rightViewController withAttributes:[self finalLayoutAttributesForViewController:self.sideViewsController.rightViewController]];
        if (rightAttributes) attributes[rightAttributes.decorationViewIdentifier] = rightAttributes;
    }
    
    return attributes;
}
- (APESideViewsLayoutAttributes *)layoutAttributesForDecorationViewOfKind:(NSString *)kind forViewController:(UIViewController *)viewController {
    if ([kind isEqualToString:APEOverlayLayoutLeftHairlineDecoration] || [kind isEqualToString:APEOverlayLayoutRightHairlineDecoration]) {
        APESideViewsLayoutAttributes *attributes = [self layoutAttributesForViewController:viewController];
        return [self attributesForHairlineForViewController:viewController withAttributes:attributes];
    } else {
        return nil;
    }
}
- (APESideViewsLayoutAttributes *)finalLayoutAttributesForDecorationViewOfKind:(NSString *)kind forViewController:(UIViewController *)viewController {
    if ([kind isEqualToString:APEOverlayLayoutLeftHairlineDecoration] || [kind isEqualToString:APEOverlayLayoutRightHairlineDecoration]) {
        APESideViewsLayoutAttributes *attributes = [self finalLayoutAttributesForViewController:viewController];
        return [self attributesForHairlineForViewController:viewController withAttributes:attributes];
    } else {
        return nil;
    }
}

#pragma mark - Private methods
- (APESideViewsLayoutAttributes *)attributesForSideViewController:(UIViewController *)viewController showing:(BOOL)showing {
    
    if (showing) {
        
        CGSize mainSize = [self mainSize];
        
        CGRect frame;
        frame.size.width = MIN(viewController.preferredContentSize.width, mainSize.width);
        frame.size.height = mainSize.height;
        frame.origin.y = (mainSize.height-frame.size.height)/2.0;
        
        if (viewController == self.sideViewsController.leftViewController) {
            frame.origin.x = 0;
        } else {
            frame.origin.x = mainSize.width - frame.size.width;
        }
        
        return [[APESideViewsLayoutAttributes alloc] initWithFrame:frame transform3d:CATransform3DIdentity zIndex:1 alpha:1 decorationViewIdentifier:nil];
    } else {
        return nil;
    }
}
- (APESideViewsLayoutAttributes *)attributesForHairlineForViewController:(UIViewController *)viewController withAttributes:(APESideViewsLayoutAttributes *)attributes {
    
    if (viewController == nil || attributes == nil) {
        return nil;
    }
    
    APESideViewsMutableLayoutAttributes *newAttributes = [attributes mutableCopy];
    if (viewController == self.sideViewsController.leftViewController) {
        newAttributes.decorationViewIdentifier = APEOverlayLayoutLeftHairlineDecoration;
    } else {
        newAttributes.decorationViewIdentifier = APEOverlayLayoutRightHairlineDecoration;
    }
    
    CGRect frame = attributes.frame;
    if (![attributes isHidden]) {
        CGFloat hairLineWidth = 1.0/[[UIScreen mainScreen] scale];
        if (viewController == self.sideViewsController.leftViewController) {
            frame.origin.x += frame.size.width; // Offset the hairline to the right of the view controller
        } else {
            frame.origin.x -= hairLineWidth; // Offset the hairline to the left of the view controller
        }
        frame.size.width = hairLineWidth;
        
    }
    newAttributes.frame = frame;
    
    return newAttributes;
}

@end
