Pod::Spec.new do |s|
  s.name         = "APESideViewsController"
  s.version      = "0.2.14"
  s.summary      = "A flexible container view controller for holding main content + 2 optional side bar views."
  # s.description  = <<-DESC
  #                   An optional longer description of APESideViewsController
  #
  #                   * Markdown format.
  #                   * Don't worry about the indent, we strip it!
  #                  DESC
  s.homepage     = "https://bitbucket.org/alekastrom/apesideviewscontroller"
  s.license      = 'MIT'
  # s.screenshots  = "www.example.com/screenshots_1", "www.example.com/screenshots_2"

  # Specify the license type. CocoaPods detects automatically the license file if it is named
  # 'LICENCE*.*' or 'LICENSE*.*', however if the name is different, specify it.
  # s.license      = 'MIT (example)'
  # s.license      = { :type => 'MIT (example)', :file => 'FILE_LICENSE' }

  # Specify the authors of the library, with email addresses. You can often find
  # the email addresses of the authors by using the SCM log. E.g. $ git log
  #
  s.author       = { "Alek Åström" => "alek.astrom@gmail.com" }
  s.source       = { :git => "https://bitbucket.org/alekastrom/apesideviewscontroller.git", :tag => s.version.to_s }

  s.platform     = :ios, '7.0'

  s.source_files = 'APESideViewsController', 'APESideViewsController/**/*.{h,m}'
  s.framework  = 'UIKit', 'QuartzCore'
  s.requires_arc = true

  s.xcconfig = { 'CLANG_ENABLE_MODULES' => 'YES' }

end
