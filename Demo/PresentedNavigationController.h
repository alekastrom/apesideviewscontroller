//
//  PresentedNavigationController.h
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-27.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PresentedNavigationController : UINavigationController

@end
