//
//  MainNavigationController.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-28.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "MainNavigationController.h"

@interface MainNavigationController ()

@end

@implementation MainNavigationController

- (UIStatusBarStyle)preferredStatusBarStyle {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return UIStatusBarStyleLightContent;
    } else {
        return UIStatusBarStyleDefault;
    }
}

@end
