//
//  StatusBarOverlayView.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-29.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "StatusBarOverlayView.h"

@implementation StatusBarOverlayView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor blackColor];
        self.userInteractionEnabled = NO;
    }
    return self;
}

@end
