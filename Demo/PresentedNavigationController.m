//
//  PresentedNavigationController.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-27.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "PresentedNavigationController.h"

@interface PresentedNavigationController ()

@end

@implementation PresentedNavigationController

- (CGSize)preferredContentSize {
    return CGSizeMake(276.0f, CGFLOAT_MAX);
}


@end
