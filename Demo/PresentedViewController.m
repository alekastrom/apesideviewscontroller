//
//  PresentedViewController.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-15.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "PresentedViewController.h"
#import "APESideViewsController.h"
#import "StatusBarOverlayView.h"
#import "APESideViewsSlideOverLayout.h"
#import "DemoSideViewsController.h"
#import "APESideViewsOverlayLayout.h"
#import "APESideViewsZoomLayout.h"
#import "APESideViewsFadeAnimation.h"
#import "APESideViewsOverlaySlideAnimation.h"
#import "APESideViewsOverlaySkewLayout.h"

@interface PresentedViewController()
@property (weak, nonatomic) IBOutlet UISlider *dampeningSlider;
@property (weak, nonatomic) IBOutlet UISlider *durationSlider;
@property (weak, nonatomic) IBOutlet UISwitch *overlaySwitch;
@property (weak, nonatomic) IBOutlet UISwitch *statusStyleSwitch;
@property (weak, nonatomic) IBOutlet UILabel *dampeningLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@end

@implementation PresentedViewController

- (CGSize)preferredContentSize {
    return CGSizeMake(320.0f, CGFLOAT_MAX);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"Presented view controller loaded");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"Presented view controller will appear");
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"Presented view controller did appear");
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    NSLog(@"Presented view controller will disappear");
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    NSLog(@"Presented view controller did disappear");
}

- (void)willMoveToParentViewController:(UIViewController *)parent {
    [super willMoveToParentViewController:parent];
    NSLog(@"Presented view controller will move to parent: %@", parent);
}

- (void)didMoveToParentViewController:(UIViewController *)parent {
    [super didMoveToParentViewController:parent];
    NSLog(@"Presented view controller did move to parent: %@", parent);
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    NSLog(@"Presented view controller will rotate");
}

- (IBAction)hideView:(id)sender {
    [self.sideViewsController setShowing:NO sideViewController:self.navigationController animated:YES];
}

#pragma mark - User interaction
- (IBAction)dampeningChanged:(UISlider *)sender {
    self.dampeningLabel.text = [NSString stringWithFormat:@"Spring dampening: %.2f", sender.value]
    ;
    ((DemoSideViewsController *)self.sideViewsController).animation.showDampening = sender.value;
}
- (IBAction)overlayToggled:(UISwitch *)sender {
    if (sender.on) {
        [self.sideViewsController.layout registerClass:[StatusBarOverlayView class] forDecorationViewWithIdentifier:APESlideOverLayoutStatusBarDecoration];
    } else {
        [self.sideViewsController.layout registerClass:NULL forDecorationViewWithIdentifier:APESlideOverLayoutStatusBarDecoration];
    }
}
- (IBAction)childStatusBarToggled:(UISwitch *)sender {
    self.sideViewsController.layout.useStatusBarStyleFromPresentedSideViewController = sender.on;
}
- (IBAction)durationChanged:(UISlider *)sender {
    self.durationLabel.text = [NSString stringWithFormat:@"Duration: %.2f", sender.value];
    ((DemoSideViewsController *)self.sideViewsController).animation.duration = sender.value;
}

#pragma mark - Tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        // New layout
        APESideViewsLayout *layout = nil;
        switch (indexPath.row) {
            case 0:
                layout = [[APESideViewsOverlayLayout alloc] initWithSideViewsController:self.sideViewsController];
                break;
            case 1:
                layout = [[APESideViewsSlideOverLayout alloc] initWithSideViewsController:self.sideViewsController];
                break;
            case 2:
                layout = [[APESideViewsZoomLayout alloc] initWithSideViewsController:self.sideViewsController];
                break;
            case 3:
                layout = [[APESideViewsOverlaySkewLayout alloc] initWithSideViewsController:self.sideViewsController];
                break;
            default:
                break;
        }
        layout.useStatusBarStyleFromPresentedSideViewController = self.statusStyleSwitch.on;
        [self overlayToggled:self.overlaySwitch];
        
        [self.sideViewsController setLayout:layout animated:YES];
    } else if (indexPath.section == 1) {
        // New animation
        APESideViewsViewBasedAnimation *animation = nil;
        switch (indexPath.row) {
            case 0:
                animation = [APESideViewsViewBasedAnimation new];
                break;
            case 1:
                animation = [APESideViewsFadeAnimation new];
                break;
            case 2:
                animation = [APESideViewsOverlaySlideAnimation new];
                break;
            case 3:
                animation = [APESideViewsZoomLayout zoomAnimation];
                break;
            default:
                break;
        }
        animation.duration = self.durationSlider.value;
        animation.showDampening = self.dampeningSlider.value;
        ((DemoSideViewsController *)self.sideViewsController).animation = animation;
    }
    
}

@end
