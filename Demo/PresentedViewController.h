//
//  PresentedViewController.h
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-15.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PresentedViewController : UITableViewController

@end
