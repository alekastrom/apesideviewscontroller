//
//  MainViewController.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-15.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "MainViewController.h"
#import "APESideViewsController.h"
#import "APEHairlineView.h"
#import "APESideViewsOverlayLayout.h"

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        
    NSLog(@"Main view controller loaded");

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"Main view controller will appear");
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"Main view controller did appear");
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    NSLog(@"Main view controller will disappear");
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    NSLog(@"Main view controller did disappear");
}

- (void)willMoveToParentViewController:(UIViewController *)parent {
    [super willMoveToParentViewController:parent];
    NSLog(@"Main view controller will move to parent: %@", parent);
}

- (void)didMoveToParentViewController:(UIViewController *)parent {
    [super didMoveToParentViewController:parent];
    NSLog(@"Main view controller did move to parent: %@", parent);
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    NSLog(@"Main view controller will rotate");
}

- (IBAction)showLeft:(id)sender {
    [self.sideViewsController setShowingLeftViewController:YES animated:YES];
}

- (IBAction)showRight:(id)sender {
    [self.sideViewsController setShowingRightViewController:YES animated:YES];
}

@end
