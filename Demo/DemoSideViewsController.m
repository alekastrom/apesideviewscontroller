//
//  DemoSideViewsController.m
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-28.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "DemoSideViewsController.h"
#import "APESideViewsZoomLayout.h"
#import "APESideViewsSlideOverLayout.h"
#import "APESideViewsOverlaySlideAnimation.h"

@interface DemoSideViewsController () <APESideViewsControllerDelegate>


@end

@implementation DemoSideViewsController

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.animation = [APESideViewsOverlaySlideAnimation new];
        self.delegate = self;
    }
    return self;
}

- (void)willPerformOperation:(APESideViewsControllerOperation)operation onViewController:(UIViewController *)viewController {
    self.mainViewController.view.tintAdjustmentMode = (operation == APESideViewsControllerOperationShowSideView) ? UIViewTintAdjustmentModeDimmed : UIViewTintAdjustmentModeAutomatic;
}

- (void)didPerformOperation:(APESideViewsControllerOperation)operation onViewController:(UIViewController *)viewController {
    self.mainViewController.view.userInteractionEnabled = (operation == APESideViewsControllerOperationHideSideView);
}

- (id<APESideViewsAnimatedTransitioning>)sideViewsController:(APESideViewsController *)sideViewsController animationControllerForOperation:(APESideViewsControllerOperation)operation onViewController:(UIViewController *)viewController {
    return self.animation;
}
@end
