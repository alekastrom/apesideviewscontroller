//
//  DemoSideViewsController.h
//  APESideViewsController
//
//  Created by Alek Åström apegroup on 2013-09-28.
//  Copyright (c) 2013 Apegroup. All rights reserved.
//

#import "APESideViewsController.h"
#import "APESideViewsViewBasedAnimation.h"

@interface DemoSideViewsController : APESideViewsController

@property (nonatomic, strong) APESideViewsViewBasedAnimation *animation;

@end
